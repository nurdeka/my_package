# setup.py
from setuptools import setup, find_packages

setup(
    name="my_first_package",
    version="0.1.0",
    packages=find_packages(),
    install_requires=[],  # List your dependencies here
    url="https://github.com/yourusername/my-package",
    license="MIT",
    author="Nurdeka Hidayanto",
    author_email="nurdeka.bmkg@gmail.com",
    description="A simple Python package",
)
